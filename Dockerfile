FROM        golang:1.10-alpine as build
RUN         apk add --no-cache curl \
            && curl -L https://download.docker.com/linux/static/stable/x86_64/docker-18.03.1-ce.tgz > /docker-18.03.1-ce.tgz \
            && cd / \
            && tar xvzf /docker-18.03.1-ce.tgz
WORKDIR     /go/src
ENV         CGO_ENABLED=0
ENV         GO_PATH=/go/src
COPY        . /go/src
RUN         go build -a --installsuffix cgo --ldflags=-s -o proxainer

FROM        scratch
COPY        --from=build /go/src/proxainer /
COPY        --from=build /docker/docker /
ENTRYPOINT  ["/proxainer"]

